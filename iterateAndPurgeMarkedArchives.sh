#!/bin/bash
#loops given directory looking for files marked with a
# .PURGE_PROPERTIES
# that should have been created by calling script
#
# called by: launchBackupDumpAndPurge.sh

PFLNAME=.PURGE_PROPERTIES
#
# VALIDATe we get two args, path and log file
# and name of backup source subfolder
if [ "x$backupTarget" == "x" ]
then
 printf "\n\tYOu may not call this file directly, it must be sourced from launchBackupDumpAndPurge.sh(parent script)"
 exit 99
 fi
ARCHVDIR=$backupTarget #full source path
mLINE="=-----------------------------------------------------------------------------="
filesPurged=0

cd ${ARCHVDIR}/
for DIR in $(ls | grep ^[a-z._-]*$) 
do
	printf "\n%s\n%s\n" ${DIR} $mLINE
			# 
	DAYSTOLIVE=2 #really low defaul
	# include properties
	if [ -f $DIR/${PFLNAME} ]
	then		
		. $DIR/${PFLNAME}
	else
		printf "\tWARN: $DIR does not have a ${PFLNAME} file setting retention, $DAYSTOLIVE days will be preserved.\n"
	fi
		# delete responsibly! (note this is -mtime while the purge is +mtime)
                filesLeft=`find $DIR -maxdepth 2 -mtime -${DAYSTOLIVE} -regextype posix-extended -iregex '(.*\.tar|.*\.gz|.*\.rsync)' | wc -l`
                if [ ${filesLeft} -lt 4 ]
                then
                        printf "\tWARNING: only %d backups(sql and files combined) within retention for %s!\n\tTHis script will not purge such a sparse directory\n" $filesLeft $DIR
                else
                        printf "\tINFO: $filesLeft files are within retention period for backup $DIR\n"
                        printf "\tMax Days to Live: %d\n" ${DAYSTOLIVE}

                        tmpcnt=`find $DIR -maxdepth 2 -mtime +${DAYSTOLIVE} -regextype posix-extended -iregex '(.*\.tar|.*\.gz|.*\.rsync)' | wc -l`
                        filesPurged=$(( $filesPurged + $tmpcnt ))
                        #find $DIR -mtime +${DAYSTOLIVE} -iname \*.gz -exec echo {} \; 
                        find $DIR -maxdepth 2 -mtime +${DAYSTOLIVE} -regextype posix-extended -iregex '(.*\.tar|.*\.gz|.*\.rsync)' -exec rm -R {} \; -exec printf "\t%s\n" {} \;
			printf "\t%0d archives purged\n" $tmpcnt
		fi
	printf "%s\n" $mLINE
done
#calling script should see %filesPurged
