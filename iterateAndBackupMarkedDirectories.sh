#!/bin/bash
#loops provdided path, looks for dirrectories marked for backup, tarballs, and if db props present, backups a db.
#
# called by launchBackupDumpAndPurge.sh
mLINE="=------------------------------------------------------------------------------="
#g
#
#
BU_FILE_COUNT=0
BU_DB_COUNT=0
#
# and name of backup source subfolder
if [ $# -ne 2 ]
then
 printf "\n\tUsage: scanAndHandle sourceDir TargetDir  (where both are relative under the curren tuser)\n\n"
 exit 99
 fi
WEBDIR=$1 #full source path
DESDIR=$2 #full destination path
#
# and name of dest folder for tar files
cd ${WEBDIR}/

#DIRS=`ls | grep ^[a-z.]*$`
#DIRS=( `ls | grep ^[a-z.]*$ | tr "\n" " "` )
#echo ${DIRS[*]}
TODAY=`date`
BU_FILE_COUNT=0
suffix=$(date +%Y-%m-%d)


for DIR in $(ls | grep ^[a-z._-]*$) 
do
	printf "\n$DIR\n"
	#tar the current directory
	if [ -f $DIR/.BACKUP_PROPERTIES ]
	then		
		p=$( stat --format=%a $DIR/.BACKUP_PROPERTIES )
		if [ $p -ne "0600" ]
		then
			printf "%s \n\t %s \n %s \n" $hLINE "WARNING: File permission modified to be 0600, but if the file contained sensitive information it may be too late, you should change the DB user and password for this application" $hLINE 
			chmod 0600 $DIR/.BACKUP_PROPERTIES 
		fi
		printf "%s\n\tBacking up files in %s, .BACKUP_PROPERTIES present \n" $mLINE ${DIR}
		tarpath=${DESDIR}/${DIR}/files
		sqlpath=${DESDIR}/${DIR}/sql
		#
		#check if we need to make path
		#
		if [ -d $tarpath ]
		then
			# direcotry exists, we're good to continue
			filler="umin"
		else
			printf "\t\tCreating %s\n" $tarpath
			mkdir -p $tarpath
			# add purge file to archive
			printf "\t\tAdding PURGE_PROPERTIES file with Max Days to Live: 5\n"
			printf "#created by %s \n DAYSTOLIVE=5\n" $0 > ${DESDIR}/${DIR}/.PURGE_PROPERTIES
		fi
		siteSize=`du -s $DIR| tr -s " " | cut -f1`
                if [ $siteSize -lt 1000000 ]
                then
                        # gzip sites < 1G
                        tar -zcf ${tarpath}/${DIR}_$suffix.tar.gz ./$DIR
                else
                        printf "\tNot gzipping site with size of $siteSize, uncompressed tarball instead\n"
                        #cp -ar ./$DIR ${tarpath}/${DIR}_$suffix.rsync
                        tar -cf ${tarpath}/${DIR}_$suffix.tar ./$DIR
                fi 
		BU_FILE_COUNT=$(( $BU_FILE_COUNT + 1 ))
		printf "\t\tdone\n"
		





#
		# Now the fun part, does it have a DB we should dump as well?
		#  chmod file to 0600 !!!
		# host=blah
		# user=blah
		# dbname=blah
		# pwq=blah
		#
		# 
		# include properties
		. $DIR/.BACKUP_PROPERTIES
		
		
		
		if [[ -n "$mhost" ]]
		then
		    printf "\tbacking up DB %s@%s as well\n" $mdb $mhost
			if [ ! -d $sqlpath ]
			then
				printf "\t\tCreating %s\n" $sqlpath
				mkdir -p $sqlpath
			fi
		#
			#backup db too
			SQLFILE=${sqlpath}/${mdb}_$suffix.sql.gz
			mysqldump -c -h ${mhost} -u${muser} --password=${mpw} ${mdb} 2>error | gzip > $SQLFILE
			if [ -s error ]
			then	   
				printf "\t\tWARNING: An error occured while attempting to backup %s \n\tError Details:\n\t" ${mdb} ${error}
				
				cat error
				rm -f error
			else
			
				BU_DB_COUNT=$(( $BU_DB_COUNT + 1 ))
				printf "\t\t%s was backed up successfully\n" ${db} $SQLFILE
			fi
		
		fi
		
		printf "Complete\n%s\n" $mLINE
	else
	
		
		printf "%s\nSKIPPING %s as it does not contain any .BACKUP_PROPERTIES\n%s\n\n" $mLINE $DIR $mLINE
	fi
	
done
printf "\n%0d sites backed up\n" $BU_FILE_COUNT
printf "%0d databases backed up\n" $BU_DB_COUNT
