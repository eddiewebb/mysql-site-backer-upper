#!/bin/bash
# runs a file back, sql dump, and old file purge,
#
# Calls: iterateAndBackupMarkedDirectories.sh, iterateAndPurgeMarkedArchives
# is Called By:

email=$1 #optional



backupSource=${HOME}/webroot # /home/user/<backupSource>/
backupTarget=${HOME}/supporting_files # /home/user/<backupTargetPath>/

backerUpFile=iterateAndBackupMarkedDirectories.sh
purgerFile=iterateAndPurgeMarkedArchives.sh

echo "Clearing very old logs"
find $backupTarget -maxdepth 1 -mtime +30 -regextype posix-extended -iregex '(.*\.log)' -exec rm -R {} \; -exec printf "\t%s\n" {} \;

#function allows recursion, could have easily looped i relaize now
getLog(){

	attempt=${2:-0}

	TODAY=`date`
	suffix=$(date +%Y-%m-%d)
	baseFilePath=$1/backup_${suffix}

	if [ $attempt -gt 25 ]
		then
		echo "Could not create log file after $attempt attempts using base $baseFilePath">&2
		exit 99
	elif [ $attempt -eq 0 ]
		then
		potential=$baseFilePath.log
	else
		potential=${baseFilePath}.${attempt}.log
	fi
	echo "Attempting to use log path : $potential"
	if [ -s $potential ]
		then
		let newattempt=$attempt+1
		getLog $1 $newattempt
	fi
	logPath=$potential

}


getLog $backupTarget


#rediret all output
touch $logPath
exec 1<> $logPath
exec 2<> $logPath


#useful things well want later
scriptDir=`dirname $0`
startDir=`pwd`


hLINE="\*******************************************************************************"

#
# Check for required files and dirs
#
if [ ! -f $scriptDir/$backerUpFile ]
then
	echo "Make sure all packaged files are extracted together. Missing $backerUpFile"
	exit 1
fi
if [ ! -f $scriptDir/$purgerFile ]
then
	echo "Make sure all packaged files are extracted together. Missing $purgerFile"
	exit 1
fi
if [ ! -d $backupSource ]
then
	echo "The source directory $backupSource could not be found, and must include nested site directories"
	exit 1
fi



#
# Start backup on source directory of any diretories that contain a .BACKUP_PROPERTIES file
#
machine=`hostname`
printf "\n%s\nBeginning backup & purge on ${USER}@${machine}\n" $hLINE
printf "\t Starting with backup.."
printf "\tsub-directories of %s (with a .BACKUP_PROPERTIES file)\n" $backupSource
printf "%s\n\n" $hLINE
source $scriptDir/$backerUpFile $backupSource $backupTarget



#
# Start to purge backupTarget for any files that exceed the count set in each backup's 
#
printf "\n%s\nNow Purging Old Backups...\n" $hLINE 
printf "\tUsing %s as backup archive" ${backupTarget}
printf "\n%s\n" $hLINE
cd $startDir
source $scriptDir/$purgerFile


# if the file remoteBackup.properties is present and contains the values we need, attempt to rysnc site_backups dir to remote.
#
# NOte: sourcing executes and exposes reporting variables.
printf "\n%s\nSYncing to remote backup server (if configured)...\n" $hLINE 
printf "\n%s\n" $hLINE
cd $startDir
source ./moveBackupsToRemote.sh ${backupTarget}
#RSYNC_EXIT : the exit code from rsync  (see http://wpkg.org/Rsync_exit_codes)






printf "%s\nBackup Report for \n\tlogfile: %s\n" $hLINE $logPath | tee message.txt
printf "%00d Tarballs created\n" $BU_FILE_COUNT| tee -a message.txt
printf "%00d Database created\n" $BU_DB_COUNT| tee -a message.txt
printf "%00d files removed\n" $filesPurged| tee -a message.txt
printf "%00d Files (DB & tar) uploaded to remote\n" $filesUploaded| tee -a message.txt
printf "%00d Files (DB & tar) purged on remote\n" $rFilesDeleted| tee -a message.txt
printf "%s\n" $hLINE| tee -a message.txt



#
# Run validattion rules to send out emails as needed.
#
#
# Check that the total files purged is == to the run interval * the files created each run
#


expected=$( echo "$BU_FILE_COUNT + $BU_DB_COUNT" | bc )
machine=`hostname`
if [ $expected -ne $filesPurged ]
then
	echo "WARNING: Expected count for $3 run does not match"
	echo "reasoning: $filesPurged != ($BU_FILE_COUNT + $BU_DB_COUNT)"
	subject="Website backup FAILED on ${USER}@$machine"
	echo "The number of files deleted does not meet the expected count based on new files created">>message.txt
	echo " ( $filesPurged deleted != ($BU_FILE_COUNT tar + $BU_DB_COUNT db )created ) ">>message.txt 
else
	echo "SUCCESS: Expected count for backups and purge on $3 matches"
	subject="Website backup success on ${USER}@$machine"
	echo "The number of files deleted matches the expected count based on new files created">>message.txt
	echo " ( $filesPurged deleted == ($BU_FILE_COUNT tar + $BU_DB_COUNT db )created ) ">>message.txt 
fi


if [ "x$RSYNC_TRY" != "x" ]
	then
		printf "\nRSYNC results: %s\n Exited with code : %d\n" "$RSYNC_TRY" $RSYNC_EXIT >> message.txt
	fi

if [ "${email/@}" != "$email" ]
    then
		mutt -s "$subject" -a $logPath -- $email < message.txt
fi
rm message.txt
