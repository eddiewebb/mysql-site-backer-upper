# MySql Site Backup (includes files)
Creates a tar ball of current site files and gzip SQL dump of database while purging older archives that exceed retention period.


I suggest daily or weekly runs.  Each created archive directory will contain an .PURGE_PROPERTIES that defines the *days* to keep backups. not the count.  Altough the system will always leave atleast 4 backups (2 tar, 2 sql) you should carefulyl consider your interval and retention.

If you are considered with a relatively static site, use high interval and retention (few copies goign back far).

If you are using a highly dynamic site where the user may shoot themselves in the foot, use frequent and short retention (many copies, but none too old)

| Interval | DAYSTOKEEP | will result in.. |
| daily | 3-14 | 6 - 28 files |
| weekly | 14-30 | 4 - 8 files |
| monthly | 60-90  | 4 - 6 files |

## Install
### Initialize the git repo/master script directory 
Use https url for anonymous read access, this allows updates over time.
```
cd ~/scripts
git clone https://eddiewebb@bitbucket.org/eddiewebb/mysql-site-backer-upper.git
```
### Setup cron job 
Use the sample launch.sh.sample as recommended to checkout first & execute to get updates over time.
```
# cron entry to run daily at midnight
0 0 * * * /home/yourusername/scripts/mysql-site-backer-upper/launch.sh
```

Or you could call the the commands below
```
cd /home/you/scripts/mysql-site-backer-upper && git pull
/home/you/scripts/mysql-site-backer-upper/launchBackupDumpAndPurge.sh you@gmail.com
```


## Usage
### Directory Structure ! important
The source for backups must have:
1. A subfolder for each site, ideally uding the domain name 'mydomain.com'
1. A file under each sub-directory (.BACKUP_PROPERTIES) that defines config for each site. 

```
/home/user/webroot
    - mydomain1.com
            - .BACKUP_PROPERTIES
    - skip-this-domain1.com
    - mydomain-alsobackedup.com
            - .BACKUP_PROPERTIES
    - some-randomc_folder_ignored
```

```
$ /home/path/to/script/launchBackupDumpAndPurge.sh emailfor@errors.com
```

## Configuration
### overall configuration
Edit launchBackupAndPurge to edit
1. Source folder containing sites
1. Backup folder to place archives
1. Log file pattern to store results

### domain config
Edit the .BACKUP_PROPERTIES to define the settings below.
```
#!/bin/bash
# DB is associated with this site and should be backed up.
mdb=db_instance_name
muser=dbusername
mpw=dbpassword
mhost=mysql.domain.com
```
### Piurge config (.PURGE_PROPERTIES)
This file will be created witha  default value initially, and should be edited to meet your interval needs.
```
#created by ./scripts/master/iterateAndBackupMarkedDirectories.sh 
 DAYSTOLIVE=5
```

## Archive Directory
The archive site mirrors the source folder, but wil SQL and files divided.
```
/home/user/site_backups/
├── 2014-11-22_purge.log
├── 2014-11-23_purge.log
└── mydomain.com
    ├── .PURGE_PROPERTIES
    ├── files
    │   ├── mydomain.com_2014-11-22.tar.gz
    │   └── mydomain.com_2014-11-23.tar.gz
    └── sql
        ├── mydomain_wordpress_2014-11-22.sql.gz
        └── mydomain_wordpress_2014-11-23.sql.gz
```

## Optional: Configure remote backup
This file grabs mysql dump and tars the working dir but you  may want some extra protection.  You may specify an optional remote user that supports rsync.  If the script detects the remoteBackup.properties it will attempt an rsync.  You should configure and verify the remote host key ahead of time.
#### Save the required private key
Rsync uses SSH, and we require the full path to an authorized local key.  e.g.  /home/.ssh/id_backup_user
#### Define remoteBackup.properties
```
# this file should be hidden
# 
#   if present scipt will attempt to rsync, fail to scp, fail to ftp the contents of $BACKUP_DIR to remote user with folder name providwd.
#
#
ruser=username
rhost=host.domain.com
rpw=really?secret
rdir=site_backups  # folder directly under remote user's home
rid=~/.ssh/iid_key_from_last_step
```

## Error email
The system uses rudementary means to detect an overall issue, and will email the optional email address.

Each run the file creates `domains * 2`  files (assuming all files have DB configured).   Whether daily, weekly or monthly, once the DAYSTOKEEP is met (30 with weekly run would be ~ 1 month before 'settled') the count for deletion should equal the new files exactly, keeping an active window of DAYSTOKEEP.

If the system detects an imbalance (creating or deleting more files than the other), the full logs are emailed.

Example weekly runs keeping 14 days

| Days ->       | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | ..| 14| 15|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
| Runs          | x |   |   |   |   |   |   | x |   |   | x |
| Files created | 2 |   |   |   |   |   |   |  2|   |   | 2 |
| files deleted | 0 |   |   |   |   |   |   | 0 |   |   | 2 |  
| warning ?     | X |   |   |   |   |   |   | X |   |   |   |