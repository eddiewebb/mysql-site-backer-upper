#!/bin/bash


if [ -s remoteBackup.properties ]
then
	printf "\nRunning remote backup attempt!!\n\n"
	RSYNC_TRY="Yes, remoteBackup.properties found."
else
	echo "skipping remote backup, no remoteBackup.properties present"
	RSYNC_TRY="NO, create a remoteBackup.properties in $(pwd) to provide remote backups on top of local."
	pwd
	exit 1
fi

chmod 0600 remoteBackup.properties
source remoteBackup.properties

if [ "x${ruser}" == "x" ]
then
	echo "remoteBackup.properties does not contain ruser value!"
	exit 2
fi
if [ "x${rhost}" == "x" ]
then
	echo "remoteBackup.properties does not contain rhost value!"
	exit 2
fi
if [ "x${rdir}" == "x" ]
then
	echo "remoteBackup.properties does not contain rdir value!"
	exit 2
fi
if [ "x${rid}" == "x" ]
then
	echo "remoteBackup.properties does not contain rid value (path to private key identity)!"
	exit 2
fi



# sample output rsync -e'ssh -i /home/eddiesites/.ssh/id_hanjin' --delete -avR /home/eddiesites/site_backups b488244@hanjin.dreamhost.com:~/site_backups/
rsync -e"ssh -i ${rid}" -avR --delete --log-file="$logPath.rsync" --exclude=*.log --exclude=*.rsync $1 ${ruser}@${rhost}:~/${rdir}/
RSYNC_EXIT=$?

#parse reults
filesUploaded=$(grep 'f++++++++' $logPath.rsync | wc -l | tr -d " ")
rFilesDeleted=$(grep '*deleting' $logPath.rsync | wc -l | tr -d " ")

cat $logPath.rsync >> message.txt
rm $logPath.rsync
